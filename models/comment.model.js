const { Schema, model } = require('mongoose')
const { ObjectId, String } = Schema.Types
const commentSchema = new Schema({
    content: {
        requied: true,
        type: String
    },
    byUser: {
        type: ObjectId,
		ref: 'User',
		required: true
    },
    postId : {
        type: ObjectId,
		ref: 'Post',
		required: true
    }
}, { timestamps: true }
)
const Comment = model('Comment',commentSchema);
module.exports = Comment