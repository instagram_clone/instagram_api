const { Schema, model } = require('mongoose')
const { ObjectId, String } = Schema.Types
const userSchema = new Schema({
    email: {
        requied: true,
        type: String,
        unique: true
    },
    username: {
        requied: true,
        type: String,
        unique: true
    },
    password: {
        requied: true,
        type: String
    },
    profilePictureUrl: {
        requied: true,
        type: String
    },
    followers: [{type:ObjectId, ref:'User'}],
    following: [{type:ObjectId, ref:'User'}]
}, { timestamps: true }
)
const User = model('User',userSchema);
module.exports = User