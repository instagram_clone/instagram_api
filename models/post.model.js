const { Schema, model } = require('mongoose')
const { ObjectId, String } = Schema.Types
const postSchema = new Schema({
    imageUrl: {
        requied: true,
        type: String
    },
    caption: {
        requied: true,
        type: String
    },
    byUser : {
        type: ObjectId,
		ref: 'User',
		required: true
    },
    likes: [{type: ObjectId, ref:'User'}],
    comments: [{type: ObjectId, ref:'Comment'}],
}, { timestamps: true }
)

const Post = model('Post',postSchema);
module.exports = Post