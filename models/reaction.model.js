const { Schema, model } = require('mongoose')
const { ObjectId, String } = Schema.Types

const reactionSchema = new Schema({
    byUser: {
        type: ObjectId,
        ref: 'User',
        required: true
    },
    postId: {
        type: ObjectId,
        ref: 'Post',
        required: true
    }
}, { timestamps: true }
)
const Reaction = model('Reaction', reactionSchema);
module.exports = Reaction
