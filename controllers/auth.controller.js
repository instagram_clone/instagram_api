const { Exception, hashPassword, verifyPassword, getToken } = require('../utils')
const { statusCodes } = require('../config/global')
const User = require('../models/user.model');


module.exports.register = async (req, res, next) => {
    try {
        const { username, password, email } = req.body;
        const hashPw = await hashPassword(password);
        const profilePictureUrl = "https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png"
        const isExistedEmail = await User.exists({ email });
        if (isExistedEmail) throw new Exception("email is existed");
        const user = new User({
            username,
            email,
            password: hashPw,
            profilePictureUrl,
        });
        await user.save();
        delete user._doc.password;
        return res
            .status(statusCodes.OK)
            .send({
                message: "register account successful",
                user
            });
    } catch (error) {
        next(error);
    }
}

module.exports.login = async (req, res, next) => {
    try {
        const { username, password } = req.body;
        if (!username || !password) throw new Exception('username or password incorrect')
        const user = await User.findOne({ username }, { createdAt: 0, updatedAt: 0, __v: 0 })
        if (!user) throw new Exception('username or password incorrect')
        const isValidPassword = await verifyPassword(user.password, password);
        if (!isValidPassword) throw new Exception('username or password incorrect')
        delete user._doc.password
        res.status(200).send({
            user,
            token: getToken(user)
        })
    } catch (error) {
        next(error)
    }
}