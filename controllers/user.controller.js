const { Exception } = require('../utils')
const { statusCodes } = require('../config/global')
const User = require('../models/user.model');
const Post = require('../models/post.model');
const Comment = require('../models/comment.model');
module.exports.getAll = async (req, res, next) => {
    try {
        const page = req.query._page ? parseInt(req.query._page) : 1;
        const limit = req.query._limit ? parseInt(req.query._limit) : 10;
        const users = await User.find({})
            .skip((page - 1) * limit)
            .limit(limit);
        const newUsers = users.map((user) => {
            delete user._doc.password;
            return { ...user._doc }
        });
        if (newUsers.length === 0) throw new Exception("users not found");
        res.status(statusCodes.OK).send(newUsers)
    } catch (error) {
        next(error)
    }
}

module.exports.getOne = async (req, res, next) => {
    try {
        const { username } = req.params;
        const user = await User.findOne({ username }, { password: 0, createdAt: 0, updatedAt: 0, __v: 0 })
        if (!user) throw new Exception('user not found', statusCodes.NOT_FOUND)
        const posts = await Post.find({ byUser: user._id }).populate('byUser', "_id username").populate({
            path: 'comments',
            populate: { path: 'byUser', select: 'username -_id', }
        })
        if (!posts) throw new Exception('Posts from user not found', statusCodes.NOT_FOUND);
        return res.status(statusCodes.OK).send({ user, posts })
    } catch (error) {
        next(error)
    }
}

module.exports.follow = async (req, res, next) => {
    try {
        const { followId } = req.body;
        const user = await User.findByIdAndUpdate(followId, {
            $push: { followers: req.user._id }
        }, { new: true })
        if (!user) throw new Exception('user not found', statusCodes.NOT_FOUND)
        const userFollowing = await User.findByIdAndUpdate(req.user._id, {
            $push: { following: followId }
        }, { new: true }).select('-password')
        if (!userFollowing) throw new Exception('user not found', statusCodes.NOT_FOUND)
        res.status(statusCodes.OK).send(user)
    } catch (error) {
        next(error)
    }
}

module.exports.unFollow = async (req, res, next) => {
    try {
        const { unfollowId } = req.body;
        const user = await User.findByIdAndUpdate(unfollowId, {
            $pull: { followers: req.user._id }
        }, { new: true })
        if (!user) throw new Exception('user not found', statusCodes.NOT_FOUND)
        const userFollowing = await User.findByIdAndUpdate(req.user._id, {
            $pull: { following: unfollowId }
        }, { new: true }).select('-password')
        if (!userFollowing) throw new Exception('user not found', statusCodes.NOT_FOUND)
        res.status(statusCodes.OK).send(user)
    } catch (error) {
        next(error)
    }
}