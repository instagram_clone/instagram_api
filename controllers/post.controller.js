const Post = require('../models/post.model')
const Comment = require('../models/comment.model')
const { Exception } = require('../utils')
const { statusCodes } = require('../config/global')
module.exports.getAll = async (req, res, next) => {
    try {
        const page = req.query._page ? parseInt(req.query._page) : 1;
        const limit = req.query._limit ? parseInt(req.query._limit) : 10;
        const posts = await Post.find()
            .skip((page - 1) * limit)
            .limit(limit)
            .populate("byUser", 'username email profilePictureUrl')
            .populate({
                path: 'comments',
                populate: { path: 'byUser', select: 'username -_id', }
              })
            .sort({"createdAt": -1})
        // if (posts.length === 0) throw new Exception("Posts not found");
        res.status(statusCodes.OK).send(posts)
    } catch (error) {
        next(error)
    }
}
module.exports.getOne = async (req, res, next) => {
    try {
        const { id } = req.params
        const post = await Post.findOne({ _id: id })
            .populate("byUser", 'username email profilePictureUrl')
            .populate({path:'comments', select:'content byUser', populate: { path: 'byUser' , select: 'username -_id'}})
        if (!post) throw new Exception("Post not found");
        res.status(statusCodes.OK).send(post)
    } catch (error) {
        next(error)
    }
}

module.exports.createPost = async (req, res, next) => {
    try {
        const { imageUrl, caption } = req.body;
        const byUser = req.user._id;
        if( !imageUrl || !caption ) throw new Exception('Please add all fields!')
        const post = new Post({
            imageUrl,
            caption,
            byUser
        })
        await post.save();
        return res
            .status(statusCodes.OK)
            .send({
                message: "create post successful",
                post
            });
    } catch (error) {
        next(error)
    }
}

module.exports.deletePost = async (req, res, next) => {
    try {
        const post = await Post.findOne({ _id: req.params.id }).populate('byUser', '_id')
        
        if (!post) throw new Exception('Post not found!')
        if(post.byUser._id.toString() !== req.user._id.toString()) throw new Exception("User not permistion.")
        await post.remove();
        res.status(200).send({
            message: "Post deleted"
        })
    } catch (error) {
        next(error)
    }
}

module.exports.likePost = async (req, res, next) => {
    try {
        const post = await Post.findByIdAndUpdate(req.params.id, {
            $push: { likes: req.user._id }
        }, {
            new: true
        })
        if (!post) throw new Exception('Post not found!')
        res.status(200).send(post)
    } catch (error) {
        next(error)
    }
}

module.exports.unlikePost = async (req, res, next) => {
    try {
        const post = await Post.findByIdAndUpdate(req.params.id, {
            $pull: { likes: req.user._id }
        }, {
            new: true
        })
        if (!post) throw new Exception('Post not found!')
        res.status(200).send(post)
    } catch (error) {
        next(error)
    }
}

module.exports.postComment = async (req, res, next) => {
    try {
        //Find a post
        const postId = req.params.id
        const post = await Post.findOne({ _id: postId});
        if (!post) throw new Exception("Post not found!")

        //Create a Comment
        const { byUser, content } = req.body;
        const authId = req.user._id;
        if (byUser !== authId) throw new Exception('Invalid User!')
        const comment = new Comment({
            postId,
            byUser,
            content
        })
        await comment.save();
        
        //Associate Post with comment
        post.comments.push(comment._id)
        await post.save()
        res.status(statusCodes.OK).send({
            message: "Comment created!",
            comment
        })
    } catch (error) {
        next(error)
    }
}