const Comment = require('../models/comment.model');
const { Exception } = require('../utils');
const { statusCodes } = require('../config/global');


module.exports.getCommentsFromPostId = async (req, res, next) => {
    try {
        const { postId } = req.params;
        const comments = await Comment.find({ postId })
            .populate('byUser','username profilePictureUrl')
            .sort({ createAt: 1 })
        if (!comments) throw new Exception('comments not found', statusCodes.NOT_FOUND);
        return res.status(statusCodes.OK).send(comments);
    } catch (error) {
        next(error)
    }
}