const jwt = require('jsonwebtoken')
const { env } = require('../config/global')

module.exports.isAuth = (req, res, next) => {
    const token = req.headers.authorization;
    if (token) {
        const onlyToken = token.slice(7, token.length)
        jwt.verify(onlyToken, env.JWT_SECRET_KEY, function (err, decoded) {
            if (err) {
                return res.status(401).send({ msg: "Invalid Token" })
            }
            req.user = decoded
            next()
            return
        });
    } else {
        return res.status(401).send({ msg: "Token is not supplied" })
    }
}