# Instagram-clone (BE)
Clone Instagram website using MERN stack

## Requirements
- Install Node
- MongoDB server(local or cloud)

## Using
Restful server with
- Express
- Mongoose + MongoDB(MongoDB Atlas)
- Authentication with jwt
- Password with bcrypt

## Features
- API Add, Edit, Remove Post
- API login, register User
- API follow User
- API like, comment Post

## Installation
Use the package manager npm to install dependencies.
> npm i

## Run
Run with code
> npm start

Run with nodemon
> npm run dev

## Demo
[Link](https://instagram123-api.herokuapp.com/)

## Front End
[Link](https://gitlab.com/instagram_clone/instagram_react)
