const express = require('express')
const router = express.Router()
const postController = require('../controllers/post.controller')

router.get('/', postController.getAll)
router.get('/:id', postController.getOne)
router.post('/', postController.createPost)
router.delete('/:id', postController.deletePost)

router.put('/:id/like', postController.likePost)
router.put('/:id/unlike', postController.unlikePost)

router.post('/:id/comment', postController.postComment)

module.exports = router