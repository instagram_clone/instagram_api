const express = require('express')
const router = express.Router()
const userController = require('../controllers/user.controller')
const authMiddleware = require('../middleware/auth.middleware')

router.get('/', userController.getAll)
router.get('/:username', userController.getOne)
router.put('/follow', userController.follow)
router.put('/unfollow', userController.unFollow)

module.exports = router