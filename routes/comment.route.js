const express = require('express')
const router = express.Router()
const commentController = require('../controllers/comment.controller')

router.get('/:postId', commentController.getCommentsFromPostId)

module.exports = router