const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { env } = require('../config/global');
function Exception(message, statusCode) {
    this.message = message;
    this.statusCode = statusCode;
}

const hashPassword = (plainPassword) => {
    return bcrypt.hash(plainPassword, 10)
}

const verifyPassword = (hashedPassword, plainPassword) => {
    return bcrypt.compare(plainPassword, hashedPassword);
};

const getToken = (user) => {
    return jwt.sign({
        _id: user._id,
        username: user.name,
        email: user.email
    }, env.JWT_SECRET_KEY,
        { expiresIn: '12h' }
    )
}

module.exports = {
    Exception, hashPassword, verifyPassword, getToken
}