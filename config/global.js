require('dotenv').config()

module.exports = {
    env: {
        PORT: process.env.PORT,
        MONGODB_CONNECT_URI: process.env.MONGODB_CONNECT_URI,
        JWT_SECRET_KEY: process.env.JWT_SECRET_KEY
    },
    statusCodes: {
        OK: 200,
        BAD_REQUEST: 400,
        NOT_FOUND: 404,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403
    }
}