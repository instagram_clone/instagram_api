const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const { env } = require('./config/global')
const errorHandler = require('./middleware/error-handler')
// const corsMiddleware = require('./middleware/cors.middleware')
const { createConnection } = require("./db");
const userRoute = require('./routes/user.route')
const postRoute = require('./routes/post.route')
const authRoute = require('./routes/auth.route')
const commentRoute = require('./routes/comment.route')
const authMiddleware = require('./middleware/auth.middleware')


const app = express()
const port = env.PORT || 3000

// connect mongoose
createConnection()
app.use(cors())

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())






app.get('/', (req, res) => res.send('Hello World!'))
app.use('/users',authMiddleware.isAuth, userRoute)
app.use('/posts',authMiddleware.isAuth, postRoute)
app.use('/comments',authMiddleware.isAuth, commentRoute)
app.use('/auth', authRoute)

app.use(errorHandler)

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))